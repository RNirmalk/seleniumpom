package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_LoginLogout extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC001_LoginLogout";
		testDescription = "Login into leaftaps";
		testNodes = "Leads";
		author = "Gayatri";
		category = "smoke";
		dataSheetName = "LoginData";
	}
	
	@Test(dataProvider="fetchData") 
	public void login(String username, String password) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCRMLink()
		.clickLeadsLink()
		.clickCreateLeadLink()
		.enterCompanyName()
		.enterFirstName()
		.enterLastName()
		.clickCreateLead()
		.verifyFirstName();
		
		
		
		
		
		
		/*LoginPage lp = new LoginPage();
		lp.enterUsername("");
		lp.enterPassword("");
		lp.clickLogin();*/
	}
}






