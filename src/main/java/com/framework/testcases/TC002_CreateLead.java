package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC002_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription = "Login into leaftaps and Create Lead";
		testNodes = "Leads";
		author = "Nirmal";
		category = "smoke";
		dataSheetName = "LoginData";
	}
	
	@Test(dataProvider="fetchData") 
	public void login(String username, String password) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.clickCRMLink()
		.clickLeadsLink()
		.clickCreateLeadLink()
		.enterCompanyName()
		.enterFirstName()
		.enterLastName()
		.clickCreateLead()
		.verifyFirstName();
		
		
		
		
		
		
		/*LoginPage lp = new LoginPage();
		lp.enterUsername("");
		lp.enterPassword("");
		lp.clickLogin();*/
	}
}






