package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadPage extends ProjectMethods {

	
	
	
	public ViewLeadPage() {
	       PageFactory.initElements(driver, this);
		}

		@FindBy(how = How.ID,using="viewLead_firstName_sp") WebElement eleViewLeadFirstName;
		
		
		public ViewLeadPage verifyFirstName() {
		
			String ActualLeadFirstName = getTypedText(eleViewLeadFirstName);
			System.out.println(ActualLeadFirstName);
			return this;
			
		}
		
			
			
			
		}
		
		
	

