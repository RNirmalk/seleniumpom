package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyLeadsPage extends ProjectMethods {

	
	
	
	public MyLeadsPage() {
	       PageFactory.initElements(driver, this);
		}

		@FindBy(how = How.XPATH,using="//a[contains(text(),'Create Lead')]") WebElement eleCreateLeadLink;
		public CreateLeadPage clickCreateLeadLink() {
			//WebElement eleLogout = locateElement("class", "decorativeSubmit");
			click(eleCreateLeadLink);
			return new CreateLeadPage(); 
		}
	
}
