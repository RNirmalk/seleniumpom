package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	
	
	
	public CreateLeadPage() {
	       PageFactory.initElements(driver, this);
		}

		@FindBy(how = How.XPATH,using="//input[@id='createLeadForm_companyName']") WebElement eleCompanyName;
		@FindBy(how = How.XPATH,using="//input[@id='createLeadForm_firstName']") WebElement eleFirstName;
		@FindBy(how = How.XPATH,using="//input[@id='createLeadForm_lastName']") WebElement eleLastName;
		@FindBy(how = How.XPATH,using="//input[@value='Create Lead']") WebElement eleCreateLeadbutton;
		
		public CreateLeadPage enterCompanyName() {
		
			clearAndType(eleCompanyName, "Testing_Company_Nirmal");
			return this;
			
		}
		public CreateLeadPage enterFirstName() {
			clearAndType(eleFirstName, "Testing_FirstName_Nirmal");
			
			return this;
			
		}
		public CreateLeadPage enterLastName() {
			clearAndType(eleLastName, "Testing_LastName_Nirmal");
			return this;
		}
			
			public ViewLeadPage clickCreateLead() {
				click(eleCreateLeadbutton);
				return new ViewLeadPage();
			
		}
		
		}
	

